/*
 * timeUtils.h
 *
 *  Created on: Nov 18, 2019
 *      Author: ajrae
 */

#ifndef TIMEUTILS_H_
#define TIMEUTILS_H_

#include <stdint.h>

typedef struct
{
    int hours;
    int minutes;
    int seconds;
    uint16_t div;
    uint16_t ticker;
} thyme;

// Returns a 1 if the seconds have changed and 0 otherwise
int tick(thyme* clock);

int thymeEquals(thyme* lhs, thyme* rhs);

void displayThyme(thyme* clock);

#endif /* TIMEUTILS_H_ */
