#pragma once

#include "driverlib/driverlib.h"

#define TIMER_A_PERIOD  250 //T = 1/f = (TIMER_A_PERIOD * 1 us)
#define HIGH_COUNT      125  //Number of cycles signal is high (Duty Cycle = HIGH_COUNT / TIMER_A_PERIOD)

//Output pin to buzzer
#define PWM_PORT        GPIO_PORT_P1
#define PWM_PIN         GPIO_PIN7
//LaunchPad LED1 - note unavailable if UART is used
#define LED1_PORT       GPIO_PORT_P1
#define LED1_PIN        GPIO_PIN0
//LaunchPad LED2
#define LED2_PORT       GPIO_PORT_P4
#define LED2_PIN        GPIO_PIN0
//LaunchPad Pushbutton Switch 1
#define SW1_PORT        GPIO_PORT_P1
#define SW1_PIN         GPIO_PIN2
//LaunchPad Pushbutton Switch 2
#define SW2_PORT        GPIO_PORT_P2
#define SW2_PIN         GPIO_PIN6
//Input to ADC - in this case input A9 maps to pin P8.1
#define ADC_IN_PORT         GPIO_PORT_P8
#define ADC_IN_PIN_1        GPIO_PIN0
#define ADC_IN_PIN_2        GPIO_PIN1
#define ADC_IN_CHANNEL_1    ADC_INPUT_A8
#define ADC_IN_CHANNEL_2    ADC_INPUT_A9

/*
//Hall sensors
#define HALL_PORT       GPIO_PORT_P2
#define HALL_PIN_1      GPIO_PIN5
#define HALL_PIN_2      GPIO_PIN7

//Ultrasonic sensor
#define DIST_PORT       GPIO_PORT_P1
#define DIST_IN_PIN     GPIO_PIN4
#define DIST_OUT_PIN    GPIO_PIN5

//Mic
#define MIC_PORT        ADC_IN_PORT
#define MIC_PIN         ADC_IN_PIN_2
#define MIC_CHANNEL     ADC_IN_CHANNEL_2

//Speaker
#define SPEAKER_PORT    PWM_PORT
#define SPEAKER_PIN     PWM_PIN
*/

// Mic
#define MIC_PORT        ADC_IN_PORT
#define MIC_PIN         ADC_IN_PIN_2
#define MIC_CHANNEL     ADC_IN_CHANNEL_2

// Hall Sensors
#define HALL_1_PORT     GPIO_PORT_P2
#define HALL_1_PIN      GPIO_PIN7
#define HALL_2_PORT     GPIO_PORT_P2
#define HALL_2_PIN      GPIO_PIN5
#define HALL_3_PORT     GPIO_PORT_P5
#define HALL_3_PIN      GPIO_PIN0
#define HALL_4_PORT     GPIO_PORT_P1
#define HALL_4_PIN      GPIO_PIN6

// LEDs
#define LED_PORT        GPIO_PORT_P1
#define LED_EN_PORT     GPIO_PORT_P1
#define LED_EN_PIN      GPIO_PIN3
#define LED_C0_PORT     GPIO_PORT_P1
#define LED_C0_PIN      GPIO_PIN4
#define LED_C1_PORT     GPIO_PORT_P1
#define LED_C1_PIN      GPIO_PIN5

// Speaker
#define SPEAKER_PORT    PWM_PORT
#define SPEAKER_PIN     PWM_PIN

//Real Time Clock (RTC)
#define RTC_DIV         128

// Audio Parsing
#define SIMPS_BUF_SIZE  3
#define AUD_BUF_SIZE    256
#define BIG_AVG_SIZE    AUD_BUF_SIZE
#define BIG_AVG_POW     8
#define LIL_AVG_SIZE    16
#define LIL_AVG_POW     4
#define MIC_THRESHOLD   70
#define MIC_WAIT        2*AUD_BUF_SIZE

#define NUM_ZONES   4
#define NUM_HALLS   4
#define MIC_ZONE    0

#define NUM_LEDS    4

#define UART_DELAY_CYCLES 1500

void Init_GPIO(void);
void Init_Clock(void);
void Init_UART(void);
void Init_PWM(void);
void Init_ADC(void);
void Init_RTC(void);

Timer_A_outputPWMParam param; //Timer configuration data structure for PWM

void displayStringLCD(char* str);
void writeToLEDs(int led, int val);

typedef enum
{
    DISP_TIME = 0,
    DISP_ZONE1,
    DISP_ZONE2,
    DISP_ZONE3,
    DISP_ZONE4,

    /*
    //DISP_HALL_1,
    DISP_HALL_2,
    DISP_MIC,
    //DISP_DIST,
    DISP_UART,
    */

    DISP_COUNT,
} displayState;

typedef enum
{
    ZONE_ARMED = 0,
    ZONE_ALARMED,
    ZONE_OK,
    ZONE_NOT_OK,

    ZONE_COUNT
} zoneState;

