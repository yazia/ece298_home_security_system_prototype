#include "uartStrings.h"

void stringToUartInfo(uartInfo* info, char* str)
{
    info->useTime = 0;

    switch(str[0])
    {
        case 'a':
            // ARM
            if(str[1] == 'r' && str[2] == 'm' && str[3] == ' ')
            {
                info->command = CMD_ARM;
                char* temp = processHall(info, &str[4]);

                if(info->command == CMD_ARM &&
                        temp[1] == 'a' && temp[2] == 't' && temp[3] == ' ')
                {
                    processTime(info, &temp[4]);
                }
            }
            else
            {
                info->command = CMD_ERR;
            }
            break;

        case 'd':
            // DISARM
            if(str[1] == 'i' && str[2] == 's' && str[3] == 'a' &&
                    str[4] == 'r' && str[5] == 'm' && str[6] == ' ')
            {
                info->command = CMD_DISARM;
                char* temp = processHall(info, &str[7]);

                if(info->command == CMD_DISARM &&
                        temp[1] == 'a' && temp[2] == 't' && temp[3] == ' ')
                {
                    processTime(info, &temp[4]);
                }
            }
            else
            {
                info->command = CMD_ERR;
            }
            break;

        case 's':
            // SET TIME
            if(str[1] == 'e' && str[2] == 't' && str[3] == ' ' && str[4] == 't' &&
                    str[5] == 'i' && str[6] == 'm' && str[7] == 'e' && str[8] == ' ')
            {
                info->command = CMD_SET_TIME;

                processTime(info, &str[9]);
            }
            else
            {
                info->command = CMD_ERR;
            }
            break;

        default:
            info->command = CMD_ERR;
    }
}

// of form XX, XX:XX, XX:XX:XX
void processTime(uartInfo* info, char* str)
{
    info->useTime = 1;
    info->time.ticker = 0;

    info->time.hours = myatoi(str);
    if(info->time.hours < 0 || info->time.hours >= 24)
    {
        info->command = CMD_ERR;
        return;
    }

    if(str[2] == ':')
    {
        info->time.minutes = myatoi(&str[3]);
        if(info->time.minutes < 0 || info->time.minutes >= 60)
        {
            info->command = CMD_ERR;
            return;
        }

        if(str[5] == ':')
        {
            info->time.seconds = myatoi(&str[6]);
            if(info->time.seconds < 0 || info->time.seconds >= 60)
            {
                info->command = CMD_ERR;
                return;
            }
        }

    }



}

char* processHall(uartInfo* info, char* str)
{
    info->hall = atoi(str);

    if(info->hall > 4 || info->hall < 1)
    {
        info->command = CMD_ERR;
    }

    return &str[1];
}

int myatoi(const char* str)
{
    if(str[0] - '0' < 0 || str[0] - '0' > 9 ||
            str[1] - '0' < 0 || str[1] - '0' > 9)
    {
        return -1;
    }
    return ((str[0] - '0') * 10) + (str[1] - '0');
}
