/*
 * utils.h
 *
 *  Created on: Nov 18, 2019
 *      Author: ajrae
 */

#ifndef RINGBUFUTILS_H_
#define RINGBUFUTILS_H_

#include <stdint.h>
#include <stdlib.h>

// we're responsible for allocating buf to a size of capacity
typedef struct
{
    int16_t* buf;
    size_t idx;
    size_t items;
    size_t capacity;
} RingBuf;

// size should be the size of the buf[] allocated
void initRingBuf(RingBuf* ringBuf, size_t size, int16_t* buf);

// pushes val to the ring buffer and returns the replaced value
/***** WARNING return value is garbage is items < capacity *****/
int16_t ringBufPush(RingBuf* ringBuf, int16_t val);

// simpsons rule on ring buf
int16_t simpsons(RingBuf* ringBuf);

// avg
int32_t sum16bto32b(int16_t* buf, size_t num);

#endif /* RINGBUFUTILS_H_ */
