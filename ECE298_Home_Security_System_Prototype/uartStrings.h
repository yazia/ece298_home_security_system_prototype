#pragma once

#include <stdlib.h>
#include "timeUtils.h"

typedef enum
{
    CMD_ARM,
    CMD_DISARM,
    CMD_SET_TIME,
    CMD_ERR,

} uartCommand;

typedef struct
{
    uartCommand command;
    int hall;
    int useTime;

    thyme time;

} uartInfo;


void stringToUartInfo(uartInfo* info, char* str);
void processTime(uartInfo* info, char* str);
// returns a pointer to a start of the remainder of the string
char* processHall(uartInfo* info, char* str);
int myatoi(const char* str);
