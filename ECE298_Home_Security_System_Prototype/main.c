#include "main.h"
#include "ringBufUtils.h"
#include "timeUtils.h"
#include "driverlib/driverlib.h"
#include "hal_LCD.h"
#include "driverlib/rtc.h"
#include "uartStrings.h"
#include <stdio.h>
#include <string.h>
//#include <time.h>

#ifndef INCREMENT
#define INCREMENT(a, size)  a = (a + 1) - (size * (a == size - 1))
#endif  /* INCREMENT */

//Globals
volatile char g_AdcReady = 0;
volatile char g_SampleAdc = 0;
volatile char g_IncrementClock = 0;
volatile char g_ReadUart = 0;


void main()
{
    //Turn off interrupts during initialization
    __disable_interrupt();

    //Stop watchdog timer unless you plan on using it
    WDT_A_hold(WDT_A_BASE);

    // Initializations - see functions for more detail
    Init_GPIO();    //Sets all pins to output low as a default
    Init_PWM();     //Sets up a PWM output
    Init_ADC();     //Sets up the ADC to sample
    Init_Clock();   //Sets up the necessary system clocks
    Init_RTC();
    Init_UART();    //Sets up an echo over a COM port
    Init_LCD();     //Sets up the LaunchPad LCD display

    /*
     * The MSP430 MCUs have a variety of low power modes. They can be almost
     * completely off and turn back on only when an interrupt occurs. You can
     * look up the power modes in the Family User Guide under the Power Management
     * Module (PMM) section. You can see the available API calls in the DriverLib
     * user guide, or see "pmm.h" in the driverlib directory. Unless you
     * purposefully want to play with the power modes, just leave this command in.
     */
    PMM_unlockLPM5(); //Disable the GPIO power-on default high-impedance mode to activate previously configured port settings

    //All done initializations - turn interrupts back on.
    __enable_interrupt();

    char zoneStateStrings[ZONE_COUNT][16] =
    {
         "ARMED",
         "ALARM",
         "OKAY",
         "NOTOK",
    };

    ADC_startConversion(ADC_BASE, ADC_REPEATED_SINGLECHANNEL);

    char msgBuf[16];

    char uartBuf[32];
    char uartOutBuf[64];
    uartOutBuf[0] = 0;
    int uartOutIndex = 0;
    //int uartDelay = UART_DELAY_CYCLES;
    int uartIndex = 0;
    memset(uartBuf, 0, 32);

    uartInfo info;

    // ring buffers
    int16_t simpsBuf[SIMPS_BUF_SIZE];
    RingBuf simpsRing;
    initRingBuf(&simpsRing, SIMPS_BUF_SIZE, simpsBuf);
    int16_t audioBuf[AUD_BUF_SIZE];
    RingBuf audioRing;
    initRingBuf(&audioRing, AUD_BUF_SIZE, audioBuf);

    int16_t adcData = 0;
    int16_t prevData = 0;
    int startAvgs = 0;
    int32_t bigAvg = 0;
    int32_t lilAvg = 0;
    size_t lilAvgIdx;
    size_t mic_wait = 0;

    int micTriggered = 0;

    thyme clock;
    clock.hours = 0;
    clock.minutes = 0;
    clock.seconds = 0;
    clock.ticker = 0;
    clock.div = RTC_DIV;

    char lastMenuSW = 1;    // SW2
    char lastAlarmSW = 1;    // SW1

    char alarmed = 0;
    char speakerBuzzing = 0;

    displayState curState = DISP_TIME;

    zoneState zones[NUM_ZONES];
    zoneState lastZones[NUM_ZONES];

    thyme armTimes[NUM_ZONES];
    thyme disarmTimes[NUM_ZONES];
    char useArmTimes[NUM_ZONES] = {0,0,0,0};
    char useDisarmTimes[NUM_ZONES] = {0,0,0,0};

    int i;
    for(i = 0; i < NUM_ZONES; i++)
    {
        zones[i] = ZONE_ARMED;
        lastZones[i] = ZONE_ARMED;
    }

    char hallVals[NUM_HALLS];

    char ledIndex = 0;
    char update = 1;

    while(1)
    {
        /*
        sprintf(msgBuf, "BTN%d", GPIO_getInputPinValue(SW2_PORT, SW2_PIN));
        displayStringLCD(msgBuf);
        */

        ////////////////////////////////////////////////////////////////////////
        //Process signal changes

        hallVals[0] = GPIO_getInputPinValue(HALL_1_PORT, HALL_1_PIN);
        hallVals[1] = GPIO_getInputPinValue(HALL_2_PORT, HALL_2_PIN);
        hallVals[2] = GPIO_getInputPinValue(HALL_3_PORT, HALL_3_PIN);
        hallVals[3] = GPIO_getInputPinValue(HALL_4_PORT, HALL_4_PIN);

        // Clear alarms when the button is pressed
        if(GPIO_getInputPinValue(SW1_PORT, SW1_PIN) != lastAlarmSW)
        {
            lastAlarmSW = !lastAlarmSW;

            if(!lastAlarmSW)
            {
                alarmed = 0;
                for(i = 0; i < NUM_ZONES; i++)
                {
                    zones[i] = ZONE_NOT_OK;
                }
                update |= 1;
            }
        }

        // Cycle through menus
        if(GPIO_getInputPinValue(SW2_PORT, SW2_PIN) != lastMenuSW)
        {
            lastMenuSW = !lastMenuSW;

            if(!lastMenuSW)
            {
                INCREMENT(curState, (int)DISP_COUNT);
                update |= 1;
            }
        }

        // Handle zone states
        for(i = NUM_ZONES - 1; i >= 0; i--)
        {
            if(zones[i] != ZONE_ALARMED)
            {
                if(i == MIC_ZONE && micTriggered)
                {
                    if(zones[i] == ZONE_ARMED)
                    {
                        zones[i] = ZONE_ALARMED;
                        alarmed = 1;
                    }
                    else
                    {
                        zones[i] = ZONE_NOT_OK;
                    }
                }
                else if(!hallVals[i])
                {
                    if(zones[i] == ZONE_ARMED)
                    {
                        zones[i] = ZONE_ALARMED;
                        alarmed = 1;
                    }
                    else
                    {
                        zones[i] = ZONE_NOT_OK;
                    }
                }
                else
                {
                    if(zones[i] == ZONE_NOT_OK)
                    {
                        zones[i] = ZONE_OK;
                    }
                }

            }
        }

        // Play the speaker while the alarm is on
        if(alarmed)
        {
            if(!speakerBuzzing)
            {
                speakerBuzzing = 1;
                Timer_A_outputPWM(TIMER_A0_BASE, &param);
            }
        }
        else
        {
            if(speakerBuzzing)
            {
                speakerBuzzing = 0;
                Timer_A_stop(TIMER_A0_BASE);
            }
        }

        // Configure ADC
        if(g_AdcReady)
        {
            adcData = ADC_getResults(ADC_BASE);
            ringBufPush(&simpsRing, adcData);
            prevData = ringBufPush(&audioRing, simpsons(&simpsRing));
            if (!startAvgs && audioRing.items == audioRing.capacity)
            {
                startAvgs = 1;
                bigAvg = sum16bto32b(audioRing.buf, audioRing.items);
                lilAvgIdx = audioRing.items - LIL_AVG_SIZE;
                lilAvg = sum16bto32b(audioRing.buf + lilAvgIdx, LIL_AVG_SIZE);
            }
            else if (startAvgs)
            {
                bigAvg += (audioRing.buf[audioRing.idx] - prevData);
                lilAvg += (audioRing.buf[audioRing.idx] - audioRing.buf[lilAvgIdx]);
                lilAvgIdx = (lilAvgIdx + 1) % audioRing.capacity;
            }

            micTriggered = ((lilAvg >> LIL_AVG_POW) - (bigAvg >> BIG_AVG_POW) > MIC_THRESHOLD)
                    || ((lilAvg >> LIL_AVG_POW) - (bigAvg >> BIG_AVG_POW) < -MIC_THRESHOLD);

            micTriggered &= (mic_wait == MIC_WAIT);

            if (micTriggered)
                GPIO_setOutputHighOnPin(LED2_PORT, LED2_PIN);
            else
                GPIO_setOutputLowOnPin(LED2_PORT, LED2_PIN);

            mic_wait += mic_wait < MIC_WAIT;

            g_AdcReady = 0;
        }

        if(g_SampleAdc)
        {
            ADC_startConversion(ADC_BASE, ADC_REPEATED_SINGLECHANNEL);
            g_SampleAdc = 0;
        }

        // Process UART inputs
        if(g_ReadUart)
        {
            uartBuf[uartIndex] = EUSCI_A_UART_receiveData(EUSCI_A0_BASE);
            uartIndex++;

            if( uartIndex >= 32 ||uartBuf[uartIndex - 1] == 0xD ||
                    uartBuf[uartIndex - 1] == 0x4 || uartBuf[uartIndex - 1] == 0x3)
            {
                stringToUartInfo(&info, uartBuf);
                memset(uartBuf, 0, 32);
                uartIndex = 0;

                switch(info.command)
                {
                    case CMD_ERR:
                        sprintf(uartOutBuf, "\r\nINPUT ERROR\r\n");
                        uartOutIndex = 0;
                        break;

                    case CMD_ARM:
                        if(info.useTime)
                        {
                            useArmTimes[info.hall - 1] = 1;
                            armTimes[info.hall - 1].hours = info.time.hours;
                            armTimes[info.hall - 1].minutes = info.time.minutes;
                            armTimes[info.hall - 1].seconds = info.time.seconds;
                            sprintf(uartOutBuf, "\r\nScheduled arm for zone %d\r\n", info.hall);
                            uartOutIndex = 0;
                        }
                        else
                        {
                            if(zones[info.hall - 1] != ZONE_NOT_OK)
                            {
                                zones[info.hall - 1] = ZONE_ARMED;
                                sprintf(uartOutBuf, "\r\nArmed zone %d\r\n", info.hall);
                                uartOutIndex = 0;
                            }
                            else
                            {
                                sprintf(uartOutBuf, "\r\nZone not ready\r\n");
                                uartOutIndex = 0;
                            }
                        }
                        break;

                    case CMD_DISARM:
                        if(info.useTime)
                        {
                            useDisarmTimes[info.hall - 1] = 1;
                            disarmTimes[info.hall - 1].hours = info.time.hours;
                            disarmTimes[info.hall - 1].minutes = info.time.minutes;
                            disarmTimes[info.hall - 1].seconds = info.time.seconds;
                            sprintf(uartOutBuf, "\r\nScheduled disarm for zone %d\r\n", info.hall);
                            uartOutIndex = 0;
                        }
                        else
                        {
                            if(zones[info.hall - 1] == ZONE_ARMED)
                            {
                                zones[info.hall - 1] = ZONE_NOT_OK;
                                sprintf(uartOutBuf, "\r\nDisarmed zone %d\r\n", info.hall);
                                uartOutIndex = 0;
                            }
                            else
                            {
                                sprintf(uartOutBuf, "\r\nZone not armed\r\n");
                                uartOutIndex = 0;
                            }
                        }
                        break;

                    case CMD_SET_TIME:
                        update |= (curState == DISP_TIME);
                        clock.hours = info.time.hours;
                        clock.minutes = info.time.minutes;
                        clock.seconds = info.time.seconds;
                        clock.ticker = 0;
                        sprintf(uartOutBuf, "\r\nUpdated time\r\n");
                        uartOutIndex = 0;
                        break;
                }
            }
            g_ReadUart = 0;
        }

        if(g_IncrementClock)
        {
            writeToLEDs(ledIndex, zones[ledIndex] == ZONE_ALARMED);
            INCREMENT(ledIndex, NUM_LEDS);
            update |= (tick(&clock) && (curState == DISP_TIME));
            g_IncrementClock = 0;

            if(uartOutBuf[uartOutIndex])
            {
                EUSCI_A_UART_transmitData(EUSCI_A0_BASE, uartOutBuf[uartOutIndex]);
                uartOutIndex++;
            }
        }

        for(i = NUM_ZONES - 1; i >= 0; i--)
        {
            if(useArmTimes[i] && thymeEquals(&clock, &armTimes[i]))
            {
                zones[i] = ZONE_ARMED;
                useArmTimes[i] = 0;
            }
            else if(useDisarmTimes[i] && thymeEquals(&clock, &disarmTimes[i]))
            {
                zones[i] = ZONE_NOT_OK;
                useDisarmTimes[i] = 0;
            }
        }

        for(i = NUM_ZONES - 1; i >= 0; i--)
        {
            update |= (zones[i] != lastZones[i]);
            lastZones[i] = zones[i];
        }

        if(update)
        {
            switch(curState)
            {
                case DISP_TIME:
                    displayThyme(&clock);
                    break;

                case DISP_ZONE1:
                    sprintf(msgBuf, "1%s", zoneStateStrings[zones[0]]);
                    displayStringLCD(msgBuf);
                    break;

                case DISP_ZONE2:
                    sprintf(msgBuf, "2%s", zoneStateStrings[zones[1]]);
                    displayStringLCD(msgBuf);
                    break;

                case DISP_ZONE3:
                    sprintf(msgBuf, "3%s", zoneStateStrings[zones[2]]);
                    displayStringLCD(msgBuf);
                    break;


                case DISP_ZONE4:
                    sprintf(msgBuf, "4%s", zoneStateStrings[zones[3]]);
                    displayStringLCD(msgBuf);
                    break;
                default:
                    displayStringLCD("ERROR");
                    break;
            }
        }

        update = 0;

    }
}


const size_t LCD_POSITION_COUNT = 6;
const size_t positions[] = {pos1, pos2, pos3, pos4, pos5, pos6};

void displayStringLCD(char* str)
{
    int done = 0;
    size_t i = 0;

    while(!done && i < LCD_POSITION_COUNT)
    {
        if(str[i] == 0)
        {
            done = 1;
            showChar(' ', positions[i]);
        }
        else
        {
            showChar(str[i], positions[i]);
        }
        i++;
    }

    while(i < LCD_POSITION_COUNT)
    {
        showChar(' ', positions[i]);
        i++;
    }
}



void writeToLEDs(int led, int val)
{
    // Convert index from room order to board order
    led = (led + 3) % 4;    // From left to right: 3, 0, 1, 2

    int c0 = led & 0x1;
    int c1 = led & 0x2;

    if(val)
    {
        if(c0)
        {
            GPIO_setOutputHighOnPin(LED_C0_PORT, LED_C0_PIN);
        }
        else
        {
            GPIO_setOutputLowOnPin(LED_C0_PORT, LED_C0_PIN);
        }

        if(c1)
        {
            GPIO_setOutputHighOnPin(LED_C1_PORT, LED_C1_PIN);
        }
        else
        {
            GPIO_setOutputLowOnPin(LED_C1_PORT, LED_C1_PIN);
        }

        GPIO_setOutputLowOnPin(LED_EN_PORT, LED_EN_PIN);
    }
    else
    {
        GPIO_setOutputHighOnPin(LED_EN_PORT, LED_EN_PIN);
    }

}

void Init_GPIO(void)
{
    // Set all GPIO pins to output low to prevent floating input and reduce power consumption
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

    //Set LaunchPad switches as inputs - they are active low, meaning '1' until pressed
    GPIO_setAsInputPinWithPullUpResistor(SW1_PORT, SW1_PIN);
    GPIO_setAsInputPinWithPullUpResistor(SW2_PORT, SW2_PIN);

    //Set LED1 and LED2 as outputs
    //GPIO_setAsOutputPin(LED1_PORT, LED1_PIN); //Comment if using UART
    GPIO_setAsOutputPin(LED2_PORT, LED2_PIN);

    /*
    // For Feasibility
    GPIO_setAsInputPin(GPIO_PORT_P1, GPIO_PIN4);
    GPIO_setAsInputPin(GPIO_PORT_P2, GPIO_PIN5 | GPIO_PIN7);
    */

    // Init LEDs
    GPIO_setAsOutputPin(LED_PORT, LED_EN_PIN | LED_C0_PIN | LED_C1_PIN);

    // Init Halls
    GPIO_setAsInputPin(HALL_1_PORT, HALL_1_PIN);
    GPIO_setAsInputPin(HALL_2_PORT, HALL_2_PIN);
    GPIO_setAsInputPin(HALL_3_PORT, HALL_3_PIN);
    GPIO_setAsInputPin(HALL_4_PORT, HALL_4_PIN);

}

void Init_Clock(void)
{
    /*
     * The MSP430 has a number of different on-chip clocks. You can read about it in
     * the section of the Family User Guide regarding the Clock System ('cs.h' in the
     * driverlib).
     */

    /*
     * On the LaunchPad, there is a 32.768 kHz crystal oscillator used as a
     * Real Time Clock (RTC). It is a quartz crystal connected to a circuit that
     * resonates it. Since the frequency is a power of two, you can use the signal
     * to drive a counter, and you know that the bits represent binary fractions
     * of one second. You can then have the RTC module throw an interrupt based
     * on a 'real time'. E.g., you could have your system sleep until every
     * 100 ms when it wakes up and checks the status of a sensor. Or, you could
     * sample the ADC once per second.
     */
    //Set P4.1 and P4.2 as Primary Module Function Input, XT_LF
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P4, GPIO_PIN1 + GPIO_PIN2, GPIO_PRIMARY_MODULE_FUNCTION);

    // Set external clock frequency to 32.768 KHz
    CS_setExternalClockSource(32768);
    // Set ACLK = XT1
    CS_initClockSignal(CS_ACLK, CS_XT1CLK_SELECT, CS_CLOCK_DIVIDER_1);
    // Initializes the XT1 crystal oscillator
    CS_turnOnXT1LF(CS_XT1_DRIVE_1);
    // Set SMCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_SMCLK, CS_DCOCLKDIV_SELECT, CS_CLOCK_DIVIDER_1);
    // Set MCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_MCLK, CS_DCOCLKDIV_SELECT, CS_CLOCK_DIVIDER_1);
}

void Init_RTC(void)
{
    RTC_init(RTC_BASE, 32768/RTC_DIV, RTC_CLOCKPREDIVIDER_1);
    RTC_clearInterrupt(RTC_BASE, RTC_OVERFLOW_INTERRUPT_FLAG);
    RTC_enableInterrupt(RTC_BASE, RTC_OVERFLOW_INTERRUPT);
    RTC_start(RTC_BASE, RTC_CLOCKSOURCE_XT1CLK);
}


#pragma vector=RTC_VECTOR
__interrupt
void timer_ISR (void)
{
    uint8_t rtcStatus = RTC_getInterruptStatus(RTC_BASE, RTC_OVERFLOW_INTERRUPT_FLAG);
    g_IncrementClock = 1;
    g_SampleAdc = 1;
    RTC_clearInterrupt(RTC_BASE, rtcStatus);
}

/* UART Initialization */
void Init_UART(void)
{
    /* UART: It configures P1.0 and P1.1 to be connected internally to the
     * eSCSI module, which is a serial communications module, and places it
     * in UART mode. This let's you communicate with the PC via a software
     * COM port over the USB cable. You can use a console program, like PuTTY,
     * to type to your LaunchPad. The code in this sample just echos back
     * whatever character was received.
     */

    //Configure UART pins, which maps them to a COM port over the USB cable
    //Set P1.0 and P1.1 as Secondary Module Function Input.
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_PRIMARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN0, GPIO_PRIMARY_MODULE_FUNCTION);

    /*
     * UART Configuration Parameter. These are the configuration parameters to
     * make the eUSCI A UART module to operate with a 9600 baud rate. These
     * values were calculated using the online calculator that TI provides at:
     * http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/index.html
     */

    //SMCLK = 1MHz, Baudrate = 9600
    //UCBRx = 6, UCBRFx = 8, UCBRSx = 17, UCOS16 = 1
    EUSCI_A_UART_initParam param = {0};
        param.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
        param.clockPrescalar    = 6;
        param.firstModReg       = 8;
        param.secondModReg      = 17;
        param.parity            = EUSCI_A_UART_NO_PARITY;
        param.msborLsbFirst     = EUSCI_A_UART_LSB_FIRST;
        param.numberofStopBits  = EUSCI_A_UART_ONE_STOP_BIT;
        param.uartMode          = EUSCI_A_UART_MODE;
        param.overSampling      = 1;

    if(STATUS_FAIL == EUSCI_A_UART_init(EUSCI_A0_BASE, &param))
    {
        return;
    }

    EUSCI_A_UART_enable(EUSCI_A0_BASE);

    EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);

    // Enable EUSCI_A0 RX interrupt
    EUSCI_A_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);
}

/* EUSCI A0 UART ISR - Echoes data back to PC host */
#pragma vector=USCI_A0_VECTOR
__interrupt
void EUSCIA0_ISR(void)
{
    uint8_t RxStatus = EUSCI_A_UART_getInterruptStatus(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG);

    EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE, RxStatus);

    if (RxStatus)
    {
        EUSCI_A_UART_transmitData(EUSCI_A0_BASE, EUSCI_A_UART_receiveData(EUSCI_A0_BASE));
        g_ReadUart = 1;
    }
}

/* PWM Initialization */
void Init_PWM(void)
{
    /*
     * The internal timers (TIMER_A) can auto-generate a PWM signal without needing to
     * flip an output bit every cycle in software. The catch is that it limits which
     * pins you can use to output the signal, whereas manually flipping an output bit
     * means it can be on any GPIO. This function populates a data structure that tells
     * the API to use the timer as a hardware-generated PWM source.
     *
     */
    //Generate PWM - Timer runs in Up-Down mode
    param.clockSource           = TIMER_A_CLOCKSOURCE_SMCLK;
    param.clockSourceDivider    = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    param.timerPeriod           = TIMER_A_PERIOD; //Defined in main.h
    param.compareRegister       = TIMER_A_CAPTURECOMPARE_REGISTER_1;
    param.compareOutputMode     = TIMER_A_OUTPUTMODE_RESET_SET;
    param.dutyCycle             = HIGH_COUNT; //Defined in main.h

    //PWM_PORT PWM_PIN (defined in main.h) as PWM output
    GPIO_setAsPeripheralModuleFunctionOutputPin(PWM_PORT, PWM_PIN, GPIO_PRIMARY_MODULE_FUNCTION);
}

void Init_ADC(void)
{
    /*
     * To use the ADC, you need to tell a physical pin to be an analog input instead
     * of a GPIO, then you need to tell the ADC to use that analog input. Defined
     * these in main.h for A9 on P8.1.
     */

    //Set ADC_IN to input direction
    GPIO_setAsPeripheralModuleFunctionInputPin(MIC_PORT, MIC_PIN, GPIO_PRIMARY_MODULE_FUNCTION);

    //Initialize the ADC Module
    /*
     * Base Address for the ADC Module
     * Use internal ADC bit as sample/hold signal to start conversion
     * USE MODOSC 5MHZ Digital Oscillator as clock source
     * Use default clock divider of 1
     */
    ADC_init(ADC_BASE,
             ADC_SAMPLEHOLDSOURCE_SC,
             ADC_CLOCKSOURCE_ADCOSC,
             ADC_CLOCKDIVIDER_1);

    ADC_enable(ADC_BASE);

    /*
     * Base Address for the ADC Module
     * Sample/hold for 16 clock cycles
     * Do not enable Multiple Sampling
     */
    ADC_setupSamplingTimer(ADC_BASE,
                           ADC_CYCLEHOLD_16_CYCLES,
                           ADC_MULTIPLESAMPLESDISABLE);

    //Configure Memory Buffer
    /*
     * Base Address for the ADC Module
     * Use input ADC_IN_CHANNEL
     * Use positive reference of AVcc
     * Use negative reference of AVss
     */
    ADC_configureMemory(ADC_BASE,
                        MIC_CHANNEL,
                        ADC_VREFPOS_AVCC,
                        ADC_VREFNEG_AVSS);

    ADC_clearInterrupt(ADC_BASE,
                       ADC_COMPLETED_INTERRUPT);

    //Enable Memory Buffer interrupt
    ADC_enableInterrupt(ADC_BASE,
                        ADC_COMPLETED_INTERRUPT);
}

#pragma vector=ADC_VECTOR
__interrupt
void mic_ISR (void)
{
    uint8_t adcStatus = ADC_getInterruptStatus(ADC_BASE, RTC_OVERFLOW_INTERRUPT_FLAG);
    g_AdcReady = 1;
    ADC_clearInterrupt(ADC_BASE, adcStatus);
}
