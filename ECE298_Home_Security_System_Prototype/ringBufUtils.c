/*
 * utils.c
 *
 *  Created on: Nov 18, 2019
 *      Author: ajrae
 */

#include <ringBufUtils.h>

void initRingBuf(RingBuf* ringBuf, size_t size, int16_t* buf)
{
    memset(buf, 0, size);
    ringBuf->buf = buf;
    ringBuf->idx = 0;
    ringBuf->items = 0;
    ringBuf->capacity = size;
}

int16_t ringBufPush(RingBuf* ringBuf, int16_t val)
{
    ringBuf->idx = (ringBuf->idx + 1) % ringBuf->capacity;
    int16_t ret = ringBuf->buf[ringBuf->idx];
    ringBuf->buf[ringBuf->idx] = val;
    ringBuf->items += (ringBuf->items < ringBuf->capacity);
    return ret;
}

int16_t simpsons(RingBuf* ringBuf)
{
    int16_t* buf = ringBuf->buf;
    size_t twoBack = (ringBuf->idx + ringBuf->capacity - 2) % ringBuf->capacity;
    size_t oneBack = (ringBuf->idx + ringBuf->capacity - 1) % ringBuf->capacity;
    return (buf[twoBack] + (buf[oneBack] << 2) + buf[ringBuf->idx]) / 3;
}

int32_t sum16bto32b(int16_t* buf, size_t num)
{
    int32_t ret = 0;
    size_t i;
    for (i = 0; i < num; i++)
    {
        ret += buf[i];
    }
    return ret;
}



