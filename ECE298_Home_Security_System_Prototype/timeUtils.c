/*
 * timeUtils.c
 *
 *  Created on: Nov 18, 2019
 *      Author: ajrae
 */

#include "timeUtils.h"
#include "hal_LCD.h"
#include <stdio.h>

int tick(thyme* clock)
{
    // tick up
    clock->ticker++;
    clock->seconds += (clock->ticker == clock->div);
    clock->minutes += (clock->seconds == 60);
    clock->hours += (clock->minutes == 60);
    // zero out any overflows
    clock->hours = clock->hours * (clock->hours < 24);
    clock->minutes = clock->minutes * (clock->minutes < 60);
    clock->seconds = clock->seconds * (clock->seconds < 60);
    clock->ticker = clock->ticker * (clock->ticker < clock->div);

    return clock->ticker == 0;
}

int thymeEquals(thyme* lhs, thyme* rhs)
{
    return lhs->hours == rhs->hours     &&
           lhs->minutes == rhs->minutes &&
           lhs->seconds == rhs->seconds;
}

void showCharOr(char c, int position)
{
    if (c == ' ')
    {
        // Display space
        LCDMEMW[position/2] = 0;
    }
    else if (c >= '0' && c <= '9')
    {
        // Display digit
        LCDMEMW[position/2] = digit[c-48][0] | (digit[c-48][1] << 8);
    }
    else if (c >= 'A' && c <= 'Z')
    {
        // Display alphabet
        LCDMEMW[position/2] = alphabetBig[c-65][0] | (alphabetBig[c-65][1] << 8);
    }
    else if (c == '-')
    {
        LCDMEMW[position/2] = 0x0003;
    }
    else
    {
        // Turn all segments on if character is not a space, digit, or uppercase letter
        LCDMEMW[position/2] = 0xFFFF;
    }
}


void displayThyme(thyme* clock)
{
//    char msg[16];
//    sprintf(msg, "%d%d%d", clock->hours, clock->minutes, clock->seconds);
    showChar(clock->hours/10 + '0', pos1);
    showChar(clock->minutes/10 + '0', pos3);
    showChar(clock->seconds/10 + '0', pos5);
    showChar(clock->seconds%10 + '0', pos6);

    size_t idx = clock->hours%10;
    LCDMEMW[pos2/2] = 0x0400 | digit[idx][0] | (digit[idx][1] << 8);
    idx = clock->minutes%10;
    LCDMEMW[pos4/2] = 0x0400 | digit[idx][0] | (digit[idx][1] << 8);
}


